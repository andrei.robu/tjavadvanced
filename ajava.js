const today = new Date();


function status(response) { 
    if (response.status >= 200 && response.status < 300) { 
        return Promise.resolve(response)
    } 
    else {
        return Promise.reject(new Error(response.statusText))
    }
}
function json(response) {
     return response.json()
}
function doubleID(id){
    return id*2;
}
fetch('https://jsonplaceholder.typicode.com/users') 
    .then(status)
    .then(json) 
    .then(function(data) {
        let names = ""
        for(let i of data)
            {
                names += `
                <p style = "color : #323a56" onclick='details("${i.name}",${i.id})'>${doubleID(i.id)}</p>
                <p onclick='details("${i.name}",${i.id})'>${i.name}</p>
                <p style = "color : #323a56" onclick='details("${i.name}",${i.id})'>${data.length-8}.08.${today.getFullYear()}</p><br>`
            }
        document.querySelector(".section_users").innerHTML = names;
    }) 
    .catch(function(error) {
        console.log('bad', error); 
    })
function details(name,id){
    localStorage.setItem("selectedID",id);
    let form = `<form><br><b>Add your name:</b>`;
    form += `<br><input type="text">`;
    form += `<br><button onclick="nextPage()">${name} Details</button></form>`;
    document.querySelector(".section_details").innerHTML = form;
 
}

function nextPage(){
    localStorage.setItem("user",document.querySelector("input").value);
    window.location.href = "adv2.html";
}