document.querySelector("h1").innerHTML = localStorage.getItem("user") + ", welcome to Endava Internship 2021";
let url = 'https://jsonplaceholder.typicode.com/users/'+localStorage.getItem("selectedID");
console.log(url);
function status(response) 
    { 
    if (response.status >= 200 && response.status < 300) { 
        return Promise.resolve(response)
    } 
    else {
        return Promise.reject(new Error(response.statusText))
    }
}
function json(response) {
     return response.json()
}
function doubleID(id){
    return id*2;
}
fetch(url) 
    .then(status)
    .then(json) 
    .then(function(data) {
        setInterval(()=>{
            let names = ""
            names += 
               `<p style="color : #323a56" "font-weight: bold;">Name: </p>
                <p>${data.name}</p><br>
                <p style="color : #323a56" "font-weight: bold;">Username: </p>
                <p>${data.username}</p><br>
                <p style="color : #323a56" "font-weight: bold;">Email: </p>
                <p>${data.email}</p><br>
                <p style="color : #323a56" "font-weight: bold;">Street and Suite Address: </p>
                <p>${data.address.street},${data.address.suite}</p><br>
                <p style="color : #323a56" "font-weight: bold;">Phone: </p>
                <p>${data.phone}</p><br>
                <p style="color : #323a56" "font-weight: bold;">Company Name: </p>
                <p>${data.company.name}</p><br>
		<p style="color : #323a56">______________________________________________ </p>
		<br>`
            document.querySelector(".section_user").innerHTML = names; 
        },2000)
        
    }) 
    .catch(function(error) {
        document.querySelector("h2").innerHTML = "Oops something went wrong, please try again!" 
    })

function newDev()
{
    let form = ""
    form +=
    `<p><br></p><b>Name:</b>
    <br><input type="text" name="name" required>
    <br>
    <b>Email:</b>
    <br><input type="text" name="email" required>
    <br>
    <b>Favourite Framework:</b>
    <br><input type="text" name="framework" required>

    <br><br><button onclick="saveDev()">Save details</button>`
    document.querySelector("form").innerHTML = form; 
}

class Developer {
    constructor(name,email,favFramework = "Angular")
    {
        this.name = name;
        this.email = email;
        this.favFramework = favFramework;
    }
}

function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

function saveDev()
{
    let dev = new Developer(document.getElementsByName("name").values,document.getElementsByName("email").values,document.getElementsByName("Framework").values);
    setCookie("Dev",dev,"Tue, 02 Sep 2021 00:00:00 UTC");
}